# Startup for AccPSS:SC-IOC-001

# Load required modules
require essioc
require s7plc
require modbus
require calc

# Load standard IOC startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")
asSetFilename("pss.acf")
asSetSubstitutions("$(ASG_SET_SUBSTITUTIONS_STRINGS)")

# Load PLC specific startup script
iocshLoad("$(IOCSH_TOP)/iocsh/accpss_ncl_ctrl_gcpu_001.iocsh", "DBDIR=$(IOCSH_TOP)/db/, MODVERSION=$(IOCVERSION=)")

